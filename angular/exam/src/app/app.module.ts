import { ProductfireComponent } from './productfire/productfire.component';
import { UpdateFormComponent } from './products/updateproduct/updateproduct.component';
import { ProductsearchComponent } from './products/productsearch/productsearch.component';
import { ProductsService } from './products.service';
import { NavigationComponent } from './navigation/navigation.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { LoginComponent } from './login/login.component';
import { ProductsComponent } from './products/products.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from './../environments/environment';


//import { AngularFireModule } from 'angularfire2';
//import { AngularFireDatabaseModule } from 'angularfire2/database';
//import { environment } from './../environments/environment';
@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    LoginComponent,
    NotFoundComponent,
    NavigationComponent,
    ProductsearchComponent,
    UpdateFormComponent,
    ProductfireComponent
    
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,

     RouterModule.forRoot([
            {path: '', component: ProductsComponent},
            {path: 'login', component: LoginComponent},
            {path: 'updateproduct', component: UpdateFormComponent},
            {path: '**', component: NotFoundComponent}
          ])
  ],
  providers: [
    ProductsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
