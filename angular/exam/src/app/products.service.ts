import { environment } from './../environments/environment';
import { Injectable } from '@angular/core';
//Q2
import { Http , Headers } from '@angular/http';
import { HttpParams } from '@angular/common/http';
import { AngularFireDatabase } from 'angularfire2/database';
@Injectable()
export class ProductsService {
  http:Http; //Q2

   getProducts(){
    return this.http.get(environment.url + 'users');
  }
  postProducts(data){
    let options = {
      headers: new Headers({'content-type': 'application/x-www-form-urlencoded'})
    }
    //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
    let params = new HttpParams().append('name',data.name).append('price',data.price);
    return this.http.post(environment.url + 'products', params.toString(), options);
  }
   getProduct(id) {
    return this.http.get(environment.url + 'users/'+id);
  }

  putProduct(data,key){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('name',data.name).append('phone',data.phone);
    return this.http.put(environment.url + 'users/'+ key,params.toString(), options);
  }
    getProductsFire(){
    //הוויליו מייצר את האובזווריבל
    return this.db.list('/products').valueChanges();
  }

  
  constructor(http:Http, private db:AngularFireDatabase) { 
    this.http = http;
  }

}