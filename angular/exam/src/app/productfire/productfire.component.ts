import { ProductsService } from './../products.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'usersfire',
  templateUrl: './usersfire.component.html',
  styleUrls: ['./usersfire.component.css']
})
export class ProductfireComponent implements OnInit {

  products;

  constructor(private service:ProductsService) { }

  ngOnInit() {
    this.service.getProductsFire().subscribe(response=>{
        console.log(response);
        this.products = response;
    });
  }

}
