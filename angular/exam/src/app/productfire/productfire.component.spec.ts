import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductfireComponent } from './productfire.component';

describe('ProductfireComponent', () => {
  let component: ProductfireComponent;
  let fixture: ComponentFixture<ProductfireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductfireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductfireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
