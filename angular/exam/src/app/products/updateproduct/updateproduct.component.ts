import { ProductsService } from './../../products.service';
import { Component, OnInit , Output , EventEmitter} from '@angular/core';
import {FormGroup , FormControl, FormBuilder} from '@angular/forms';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';

@Component({
  selector: 'updateproduct',
  templateUrl: './updateproduct.component.html',
  styleUrls: ['./updateproduct.component.css']
})
export class UpdateFormComponent implements OnInit {
  @Output() updateProduct:EventEmitter<any> = new EventEmitter<any>(); 
  @Output() updateProductPs:EventEmitter<any> = new EventEmitter<any>();
  //Q4 fix
  name;
  price;

  service:ProductsService;
  product;
  updateform = new FormGroup({
    name:new FormControl(),
    price:new FormControl()
  });

  //Q4 fix router
  constructor(private route: ActivatedRoute ,service: ProductsService, private formBuilder: FormBuilder, private router: Router) {
    this.service = service;
  }

  sendData() {
    this.updateProduct.emit(this.updateform.value.name);
    console.log(this.updateform.value);

    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      this.service.putProduct(this.updateform.value, id).subscribe(
        response => {
          console.log(response.json());
          this.updateProductPs.emit();
          //Q4 fix
          this.router.navigate(['/']);
        }
      );
    })
  }
  
  ngOnInit() {
    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      console.log(id);
      this.service.getProduct(id).subscribe(response=>{
        this.product = response.json();
        console.log(this.product);
        //Q4 fix
        this.name = this.product.name
        this.price = this.product.price  
      })
    });
     //Login without JWT
    var value = localStorage.getItem('auth');
    
    if(value == 'true'){   
      //this.router.navigate(['/']);
    }else{
      this.router.navigate(['/login']);
    }
  }
}