import { ProductsService } from './../products.service';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products;
  productsKey = [];

  optimisticAdd(product){
    var newKey = parseInt(this.productsKey[this.productsKey.length - 1],0) + 1;
    var newProductObject = {};
    newProductObject['name'] = product; 
    this.products[newKey] = newProductObject;
    this.productsKey = Object.keys(this.products);
  }

  pessimisticAdd(){
    this.service.getProducts().subscribe(
      response=>{
        this.products = response.json();
        this.productsKey = Object.keys(this.products);
    })
  }
  
  constructor(private service:ProductsService) {
    service.getProducts().subscribe(
     response=>{
        this.products = response.json();
        this.productsKey = Object.keys(this.products);
    })
  }
  ngOnInit() {
  }

}
