<?php
require "bootstrap.php";

use Exam\Models\Product;

$app = new \Slim\App();
//$app->add(new Logging());
$app->get('/hello/{name}', function($request, $response, $args){
    return $response->write('Hello '.$args['name']);
 }); 

 $app->get('/products', function($request, $response, $args){
    $_product = new Product(); //create new user
    $products = $_product->all();
    $payload = [];
    foreach($products as $prd){
        $payload[$prd->id] = [
            'name'=>$prd->name,
            'price'=>$prd->price
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

$app->get('/products/{id}', function($request, $response, $args){
    $_id = $args['id'];
    $product = Product::find($_id);
       return $response->withStatus(200)->withJson($product);
    });
   

$app->post('/products', function($request, $response, $args){
    $name = $request->getParsedBodyParam('name','');
    $price = $request->getParsedBodyParam('price','');
    $_product = new Product();
    $_product->name = $name;
    $_product->price = $price;
    $_product->save();
    if($_product->id){
        $payload = ['product id: '=>$_product->id];
        return $response->withStatus(201)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});
 
$app->delete('/products/{id}', function($request, $response, $args){
    $product = Product::find($args['id']);
    $product->delete();
    if($product->exists){
        return $response->withStatus(400);
    }
    else{
         return $response->withStatus(200);
    }
});

$app->put('/products/{product_id}', function($request, $response, $args){
    $name = $request->getParsedBodyParam('name','');
    $price = $request->getParsedBodyParam('price','');
    $_product = Product::find($args['product_id']);
    //die("user id is " . $_product->id);
    $_product->name = $name;
    $_product->price = $price;
    if($_product->save()){
        $payload = ['user_id'=>$_product->id,"result"=>"The product has been updated successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
         return $response->withStatus(400);
    }
});

$app->post('/products/bulk', function($request, $response, $args){
    $payload = $request->getParsedBody();//מה שאני מקבל זה רק ג'ייסון והפקודה הופכת את פיילווד למערך 
    Product::insert($payload);
    return $response->withStatus(201)->withJson($payload);
});/*
//Login without JWT
$app->post('/login', function($request, $response,$args){
    $name  = $request->getParsedBodyParam('name','');
    $password = $request->getParsedBodyParam('password','');    
    $_user = User::where('name', '=', $name)->where('password', '=', $password)->get();
    
    if($_user[0]->id){
        $payload = ['success'=>true];
        return $response->withStatus(201)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
    }
    else{
        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }
});
*/


$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->run();